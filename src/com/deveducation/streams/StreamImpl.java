package com.deveducation.streams;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;

public class StreamImpl implements IStream {
    private List<Integer> collections;

    private Predicate<Integer> predicate;

    private Function<Integer, Integer> function;

    private List<Command> commands = new ArrayList<>();

    @Override
    public IStream filter(Predicate<Integer> predicate) {
        this.predicate = predicate;
        this.commands.add(Command.filter);
        return this;
    }

    @Override
    public IStream map(Function<Integer, Integer> f) {
        this.function = f;
        this.commands.add(Command.map);
        return this;
    }

    @Override
    public IStream sorted() {
        this.commands.add(Command.sorted);
        return this;
    }

    @Override
    public Optional<Integer> reduce(BinaryOperator<Integer> f) {
        switchCommand();
        Integer result = 0;
        for (Integer num : this.collections) {
            result = f.apply(result, num);
        }
        return Optional.of(result);
    }

    @Override
    public IStream of(Integer... numbers) {
        this.collections = Arrays.asList(numbers);
        return this;
    }

    @Override
    public long count() {
        switchCommand();
        return this.collections.size();
    }

    @Override
    public List<Integer> collectToList() {
        switchCommand();
        return collections;
    }

    @Override
    public Optional<Integer> findFirst() {
        switchCommand();
        return Optional.of(collections.get(0));
    }

    private void filter() {
        List<Integer> items = new ArrayList<>();
        for (Integer num : this.collections) {
            if (this.predicate.test(num)) {
                items.add(num);
            }
        }
        this.collections = items;
    }

    private void map() {
        List<Integer> items = new ArrayList<>();
        for (Integer num : this.collections) {
            Integer item = this.function.apply(num);
            items.add(item);
        }
        this.collections = items;
    }

    enum Command {
        filter, map, sorted
    }

    private void switchCommand() {
        for (Command c : commands) {
            switch (c) {
                case filter:
                    filter();
                    break;
                case map:
                    map();
                    break;
                case sorted:
                    Collections.sort(this.collections);
                    break;
                default:
                    break;
            }
        }
    }
}
