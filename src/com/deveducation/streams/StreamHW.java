package com.deveducation.streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamHW {

    public static void main(String[] args) {
        getStudentsByDepartment("IT");
        getStudentsByDepartmentAndCourse("IT", 3);
        getStudentsBornAfterDate ("2016-01-27");
        getStudentsByGroup (4);
        getStudentsGroupedByDepAndGroup(100);
        getStudentsCountInDepartment("Biology");

    }

     private static void getStudentsByDepartment(String department) {
        // 1) список студентов заданного факультета;
        Stream<Student> studentStream =  loadMOCKData().stream();
        studentStream
                .filter(student -> student.getDepartment().equals(department))
                .forEach(System.out::println);
        System.out.println("------------------end------------------");
    }

     private static void getStudentsByDepartmentAndCourse(String department, int course) {
        //        2) списки студентов для каждого факультета и курса;
        Stream<Student> studentStream = loadMOCKData().stream();
        studentStream
                .filter(student -> student.getDepartment().equals(department))
                .filter(student -> student.getCourse()==course)
                .map(student -> student.getDepartment() + " | " + student.getCourse() +" | "+
                        student.getFirstName() +" | "+student.getLastName())
                .sorted()
                .forEach(System.out::println);
        System.out.println("------------------end------------------");
    }

     private static void getStudentsBornAfterDate(String date) {
//        3) список студентов, родившихся после заданной даты
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
        Stream<Student> studentStream = loadMOCKData().stream();
        studentStream
                .filter(student -> {
                    try {
                        return student.getBirthDate().after(format.parse(date));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return false;
                })
                .map(student -> student.getFirstName() +" | "+student.getLastName() +" | "+
                        student.getBirthDate())
                .sorted()
                .forEach(System.out::println);
        System.out.println("------------------end------------------");
    }

     private static void getStudentsByGroup(int group) {
//        4) список учебной группы в формате Фамилия, Имя, Отчество
        Stream<Student> studentStream = loadMOCKData().stream();
        studentStream
                .filter(student -> student.getGroup()==group)
                .map(student -> student.getGroup() +"|" +student.getFirstName() +" | "+student.getLastName() +
                        " | " + student.getPatronymicName())
                .sorted()
                .forEach(System.out::println);
        System.out.println("------------------end------------------");
    }

     private static void getStudentsGroupedByDepAndGroup(int limit) {
//        5) вывести список строк в форамате Фамилия, Имя, Отчество - Факультет, Группа (List<String>);
        Stream<Student> studentStream = loadMOCKData().stream();
        studentStream
                .map(student -> student.getFirstName() +" | "+student.getLastName() +
                        " | " + student.getPatronymicName() +" - "+student.getDepartment()+" - " +student.getGroup())
                .limit(limit)
                .sorted()
                .forEach(System.out::println);
        System.out.println("------------------end------------------");
    }

     private static void getStudentsCountInDepartment(String department) {
//        6) посчитать количество студентов на заданом факультете.
        Stream<Student> studentStream = loadMOCKData().stream();

        System.out.println(studentStream
                .filter(student -> student.getDepartment().equals(department))
                .count());

    }

    private static List<Student> loadMOCKData() {

        String path = "resourses/students.csv";

        List < Student > studentList = new ArrayList<>() ;
        Student student = null;
        List<String> result = null;
        try (Stream<String> studentStream = Files.lines(Paths.get(path))) {
            result = studentStream
                    .skip(1)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        DateFormat format = new SimpleDateFormat("dd.mm.yyyy HH:mm", Locale.ENGLISH);

        for ( String record : Objects.requireNonNull(result))
        {
            String[] row = record.split(";");

            try {
                student = new Student(Integer.parseInt(row[0]), row[1], row[2], row[3], format.parse(row[4]), row[5], row[6], row[7],
                        Integer.parseInt(row[8]), Integer.parseInt(row[9]));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            studentList.add(student);
        }
        return studentList;

    }

}
