package com.deveducation.streams;

import java.util.Date;

public class Student {
    private int id;
    private String firstName;
    private String lastName;
    private String patronymicName;
    private Date birthDate;
    private String address;
    private String tel;
    private String department;
    private int course;
    private int group;


    Student(int id, String firstName, String lastName, String patronymicName, Date birthDate, String address, String tel, String department, int course, int group) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymicName = patronymicName;
        this.birthDate = birthDate;
        this.address = address;
        this.tel = tel;
        this.department = department;
        this.course = course;
        this.group = group;
    }

    int getId() {
        return id;
    }

    String getFirstName() {
        return firstName;
    }

    String getLastName() {
        return lastName;
    }

    String getPatronymicName() {
        return patronymicName;
    }

    Date getBirthDate() {
        return birthDate;
    }

    String getAddress() {
        return address;
    }

    String getTel() {
        return tel;
    }

    String getDepartment() {
        return department;
    }

    int getCourse() {
        return course;
    }

    int getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return "Student{" +
                 id +
                "|" + firstName + '\'' +
                "|" + lastName + '\'' +
                "|" + patronymicName + '\'' +
                "|" + birthDate + '\'' +
                "|" + address + '\'' +
                "|" + tel + '\'' +
                "|" + department + '\'' +
                "|" + course +
                "|" + group +
                '}';
    }
}
