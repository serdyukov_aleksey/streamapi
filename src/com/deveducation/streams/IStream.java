package com.deveducation.streams;

import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;

public interface IStream {

    IStream filter(Predicate<Integer> predicate);
    IStream sorted();
    IStream map(Function<Integer, Integer> f);
    Optional<Integer> reduce(BinaryOperator<Integer> f);
    IStream of(Integer... numbers);
    long count();
    List<Integer> collectToList();
    Optional<Integer> findFirst();


}

